<?php
namespace Bnl\Model;


use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceManager;

abstract class AbstractTable
{
    /**
     * @var TableGateway
     */
    protected $_tableGateway;

    /**
     * @var ServiceManager
     */
    protected $_serviceManager;

    /**
     * @var string
     */
    protected $_primary = null;

    /**
     * @var string
     */
    protected $_model = null;

    public function __construct(TableGateway $tableGateway, $primary, $model, ServiceManager $serviceManager = null)
    {
        $this->_tableGateway = $tableGateway;
        $this->_primary = $primary;
        $this->_model = $model;
        $this->_serviceManager = $serviceManager;
    }

    /**
     * @return ResultSet
     */
    public function fetchAll()
    {
        return $this->_tableGateway->select();
    }

    /**
     * @param $key
     *
     * @return AbstractModel
     */
    public function get($key)
    {
        $resultSet = $this->_tableGateway->select(array
        (
            $this->_primary => $key
        ));

        if ($resultSet->count() == 0)
            return new $this->_model($this->_primary, $this->_tableGateway->getTable(), $this->_tableGateway->getAdapter());

        return $resultSet->current();
    }

    /**
     * @param $key
     */
    public function delete($key)
    {
        $this->_tableGateway->delete(array($this->_primary => $key));
    }
} 