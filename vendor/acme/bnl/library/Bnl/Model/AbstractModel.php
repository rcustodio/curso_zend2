<?php

namespace Bnl\Model;


use Zend\Db\RowGateway\RowGateway;
use Zend\Filter\FilterChain;
use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;
use Zend\Validator\ValidatorChain;

abstract class AbstractModel extends RowGateway
{
    protected $_modelInputs = array();

    /**
     * @var InputFilter
     */
    protected $_inputFilter;

    public function getInputFilter()
    {
        if (empty($this->_inputFilter))
        {
            $this->_inputFilter = new InputFilter();
            foreach ($this->_modelInputs as $name => $modelInput)
            {
                $input = new Input($name);

                $filterChain = new FilterChain();
                if (isset($modelInput['filters']))
                {
                    if (is_array($modelInput['filters']) && sizeof($modelInput['filters']) > 0)
                    {
                        foreach ($modelInput['filters'] as $filter)
                            $filterChain->attach($filter);
                    }
                }
                if ($filterChain->count() > 0)
                    $input->setFilterChain($filterChain);

                $validatorChain = new ValidatorChain();
                if (isset($modelInput['validators']))
                {
                    if (is_array($modelInput['validators']) && sizeof($modelInput['validators']) > 0)
                    {
                        foreach ($modelInput['validators'] as $validator)
                            $validatorChain->attach($validator);
                    }
                }
                if ($validatorChain->count() > 0)
                    $input->setValidatorChain($validatorChain);

                if (isset($modelInput['properties']))
                {
                    if (is_array($modelInput['properties']) && sizeof($modelInput['properties']) > 0)
                    {
                        foreach ($modelInput['properties'] as $method => $value)
                        {
                            if (method_exists($input, $method))
                                $input->$method($value);
                        }
                    }
                }

                $this->_inputFilter->add($input);
            }
        }

        return $this->_inputFilter;
    }

    public function getArrayCopy()
    {
        return $this->data;
    }

    public function populate($rowData, $rowExistsInDatabase = false)
    {
        $rowData = is_array($rowData) ? $rowData : array();

        foreach ($rowData as $key => $value)
        {
            if ($key == current($this->primaryKeyColumn) && empty($value))
                $rowExistsInDatabase = false;
            else if (!isset($this->_modelInputs[$key]))
                unset($rowData[$key]);
        }

        return parent::populate($rowData, $rowExistsInDatabase);
    }
}