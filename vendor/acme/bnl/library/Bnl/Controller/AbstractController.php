<?php
namespace Bnl\Controller;

use Zend\Mvc\Controller\AbstractActionController;

abstract class AbstractController extends AbstractActionController
{
    protected $_tableName = null;

    /**
     * @param string $name
     *
     * @return mixed
     */
    protected function _getTable($name = null)
    {
        $name = is_null($name) ? $this->_tableName : $name;
        return $this->getServiceLocator()->get($name . 'Table');
    }
} 