<?php
namespace Baseball\Model;


use Bnl\Model\AbstractModel;
use Zend\Db\Adapter\Adapter;
use Zend\Filter\Null;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\I18n\Validator\Alnum;
use Zend\Validator\StringLength;

class Categoria extends AbstractModel
{
    /**
     * @var integer
     */
    public $codigo;

    /**
     * @var string
     */
    public $descricao;

    public function __construct($primary, $table, Adapter $adapter)
    {
        parent::__construct($primary, $table, $adapter);

        $this->_modelInputs = array
        (
            'codigo' => array
            (
                'properties' => array
                (
                    'setAllowEmpty' => true
                ),
                'filters' => array
                (
                    new Null()
                )
            ),
            'descricao' => array
            (
                'filters' => array
                (
                    new StringTrim(),
                    new StripTags()
                ),
                'validators' => array
                (
                    new StringLength(array('min' => 3, 'max' => 30)),
                    new Alnum()
                )
            )
        );
    }
} 