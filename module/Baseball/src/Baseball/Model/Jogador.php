<?php
namespace Baseball\Model;

use Bnl\Model\AbstractModel;
use Zend\Db\Adapter\Adapter;
use Zend\Filter\Null;
use Zend\Filter\StringToUpper;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\I18n\Validator\Alnum;
use Zend\ServiceManager\ServiceManager;
use Zend\Validator\StringLength;

class Jogador extends AbstractModel
{
    /**
     * @var integer
     */
    public $inscricao;

    /**
     * @var string
     */
    public $nome;

    /**
     * @var Categoria
     */
    public $categoria;

    /**
     * @var ServiceManager
     */
    private $_serviceManager;

    public function __construct($primary, $table, Adapter $adapter)
    {
        parent::__construct($primary, $table, $adapter);

        $this->categoria = new Categoria('codigo', 'categorias', $adapter);

        $this->_modelInputs = array
        (
            'inscricao' => array
            (
                'properties' => array
                (
                    'setAllowEmpty' => true
                ),
                'filters' => array
                (
                    new Null()
                )
            ),
            'nome' => array
            (
                'filters' => array
                (
                    new StringTrim(),
                    new StringToUpper('UTF-8'),
                    new StripTags()
                ),
                'validators' => array
                (
                    new StringLength(array('min' => 3, 'max' => 30)),
                    new Alnum()
                )
            ),
            'categoria' => array
            (
                'properties' => array
                (
                    'setAllowEmpty' => false
                )
            )
        );
    }

    public function toArray()
    {
        if (is_numeric($this->data['categoria']))
        {
            $select = $this->categoria->sql->select()->where(array('codigo = ?' => $this->data['categoria']));
            $statement = $this->categoria->sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            if ($result->count() == 1)
            {
                $categoria = new Categoria('codigo', 'categorias', $this->categoria->sql->getAdapter());
                $categoria->exchangeArray($result->current());
                $this->data['categoria'] = $categoria;
            }
        }

        return parent::toArray();
    }
} 