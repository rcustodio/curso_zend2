<?php
namespace Baseball\Form;

use Zend\Form\Element\Hidden;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Form;

class Categoria extends Form
{
    public function __construct()
    {
        parent::__construct('jogador');

        $this->setAttribute('method', 'post');

        $element = new Hidden('codigo');
        $this->add($element);

        $element = new Text('descricao');
        $element->setLabel('Descricao:');
        $element->setAttribute('autofocus', 'autofocus');
        $this->add($element);

        $element = new Submit('gravar');
        $element->setValue('Gravar');
        $this->add($element);
    }
}