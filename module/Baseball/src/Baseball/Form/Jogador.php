<?php
namespace Baseball\Form;

use Zend\Db\ResultSet\ResultSet;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Select;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Form;

class Jogador extends Form
{
    public function __construct()
    {
        parent::__construct('jogador');

        $this->setAttribute('method', 'post');

        $element = new Hidden('inscricao');
        $this->add($element);

        $element = new Text('nome');
        $element->setLabel('Nome:');
        $element->setAttribute('class', 'botao');
        $element->setAttribute('autofocus', 'autofocus');
        $this->add($element);

        $element = new Select('categoria');
        $element->setLabel('Categoria:');
        $this->add($element);

        $element = new Submit('gravar');
        $element->setValue('Gravar');
        $this->add($element);
    }

    public function setCategorias(ResultSet $categorias)
    {
        $options = array();
        $options[''] = '-- Selecione --';
        foreach ($categorias as $categoria)
        {
            $options[$categoria->offsetGet('codigo')] = $categoria->offsetGet('descricao');
        }

        $this->get('categoria')->setValueOptions($options);
    }
}