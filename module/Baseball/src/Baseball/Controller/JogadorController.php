<?php
namespace Baseball\Controller;

use Baseball\Form\Jogador as JogadorForm;
use Baseball\Model\Jogador;
use Bnl\Controller\AbstractController;
use Zend\Form\Form;
use Zend\View\Model\ViewModel;

class JogadorController extends AbstractController
{
    protected $_tableName = 'Jogador';

    public function indexAction()
    {
        $table = $this->_getTable();
        $rows = $table->fetchAll();

        return new ViewModel(array
        (
            'rows' => $rows
        ));
    }

    public function editAction()
    {
        $form = new JogadorForm();
        if (isset($_SESSION['form']))
        {
            $form = $_SESSION['form'];
            unset($_SESSION['form']);
        }
        else
        {
            $key = $this->params('key', null);
            if (!empty($key))
            {
                $table = $this->_getTable();
                $row = $table->get($key);
                $form->bind($row);
            }
        }
        $categoriaTable = $this->_getTable('Categoria');
        $form->setCategorias($categoriaTable->fetchAll());
        $form->setAttribute('action', $this->url()->fromRoute('baseball', array
        (
            'controller' => 'jogador',
            'action' => 'save'
        )));

        return array
        (
            'form' => $form
        );
    }

    public function saveAction()
    {
        $post = $this->getRequest()->getPost();
        $row = $this->getServiceLocator()->get('Baseball\Model\Jogador');

        $form = new JogadorForm();
        $form->setInputFilter($row->getInputFilter());
        $form->bind($post);

        if (!$form->isValid())
        {
            $_SESSION['form'] = $form;

            return $this->redirect()->toRoute('baseball', array
            (
                'controller' => 'jogador',
                'action' => 'edit',
                'key' => !empty($post->inscricao) ? $post->inscricao : null
            ));
        }

        $row->exchangeArray($form->getData(Form::VALUES_AS_ARRAY));
        $row->save();

        return $this->redirect()->toRoute('baseball', array
        (
            'controller' => 'jogador'
        ));
    }

    public function deleteAction()
    {
        $key = $this->params('key', null);
        $this->_getTable()->delete($key);

        $this->redirect()->toRoute('baseball', array
        (
            'controller' => 'jogador'
        ));
    }
} 