<?php
namespace Baseball\Controller;

use Baseball\Form\Categoria as CategoriaForm;
use Baseball\Model\Categoria;
use Bnl\Controller\AbstractController;
use Zend\Form\Form;
use Zend\View\Model\ViewModel;

class CategoriaController extends AbstractController
{
    protected $_tableName = 'Categoria';

    public function indexAction()
    {
        $table = $this->_getTable();
        $rows = $table->fetchAll();

        return new ViewModel(array
        (
            'rows' => $rows
        ));
    }

    public function editAction()
    {
        $form = new CategoriaForm();
        if (isset($_SESSION['form']))
        {
            $form = $_SESSION['form'];
            unset($_SESSION['form']);
        }
        else
        {
            $key = $this->params('key', null);
            if (!empty($key))
            {
                $table = $this->_getTable();
                $row = $table->get($key);
                $form->bind($row);
            }
        }
        $form->setAttribute('action', $this->url()->fromRoute('baseball', array
        (
            'controller' => 'categoria',
            'action' => 'save'
        )));

        return array
        (
            'form' => $form
        );
    }

    public function saveAction()
    {
        $post = $this->getRequest()->getPost();

        $key = $this->params('key', null);
        $row = null;
        if (!empty($key))
        {
            $table = $this->_getTable();
            $row = $table->get($key);
        }
        else
            $row = $this->getServiceLocator()->get('Baseball\Model\Categoria');

        $form = new CategoriaForm();
        $form->setInputFilter($row->getInputFilter());
        $form->bind($post);

        if (!$form->isValid())
        {
            $_SESSION['form'] = $form;

            return $this->redirect()->toRoute('baseball', array
            (
                'controller' => 'categoria',
                'action' => 'edit',
                'key' => !empty($post->codigo) ? $post->codigo : null
            ));
        }

        $row->exchangeArray($form->getData(Form::VALUES_AS_ARRAY));
        $row->save();

        return $this->redirect()->toRoute('baseball', array
        (
            'controller' => 'categoria'
        ));
    }

    public function deleteAction()
    {
        $key = $this->params('key', null);
        $this->_getTable()->delete($key);

        return $this->redirect()->toRoute('baseball', array
        (
            'controller' => 'categoria'
        ));
    }
} 