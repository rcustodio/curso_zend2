<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Baseball;

use Baseball\Model\Categoria;
use Baseball\Model\CategoriaTable;
use Baseball\Model\Jogador;
use Baseball\Model\JogadorTable;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceManager;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                    'Bnl' => realpath(__DIR__ . '/../../vendor/acme/bnl/library/Bnl')
                ),
            )
        );
    }

    public function getServiceConfig()
    {
        return array
        (
            'factories' => array
            (
                'CategoriaTable' => function(ServiceManager $serviceManager)
                {
                    /* @var $adapter AdapterInterface */
                    $adapter = $serviceManager->get('Zend\Db\Adapter');

                    $resultSet = new ResultSet();
                    $resultSet->setArrayObjectPrototype($serviceManager->get('Baseball\Model\Categoria'));

                    $tableGateway = new TableGateway('categorias', $adapter, null, $resultSet);
                    return new CategoriaTable($tableGateway, 'codigo', 'Baseball\Model\Categoria');
                },
                'JogadorTable' => function(ServiceManager $serviceManager)
                {
                    /* @var $adapter AdapterInterface */
                    $adapter = $serviceManager->get('Zend\Db\Adapter');

                    $resultSet = new ResultSet();
                    $resultSet->setArrayObjectPrototype($serviceManager->get('Baseball\Model\Jogador'));

                    $tableGateway = new TableGateway('jogadores', $adapter, null, $resultSet);
                    return new JogadorTable($tableGateway, 'inscricao', 'Baseball\Model\Jogador', $serviceManager);
                },
                'Baseball\Model\Categoria' => function(ServiceManager $serviceManager)
                {
                    /* @var $adapter AdapterInterface */
                    $adapter = $serviceManager->get('Zend\Db\Adapter');

                    return new Categoria('codigo', 'categorias', $adapter);
                },
                'Baseball\Model\Jogador' => function(ServiceManager $serviceManager)
                {
                    /* @var $adapter AdapterInterface */
                    $adapter = $serviceManager->get('Zend\Db\Adapter');

                    return new Jogador('inscricao', 'jogadores', $adapter);
                }
            )
        );
    }
}
